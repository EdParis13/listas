# DIFERENCIA ENTRE DUPLA Y LISTA, ES LO MISMO SOLO QUE LA DUPLA SE UTILIZA CON PARENTESIS.
# EN LA DUPLA NO SE PUEDE ELIMINAR NI AGREGAR VALORES, ES MAS PARA VALORES FIJOS.
if __name__ == "__main__":
    # crear lista
    lista1 = ['bah', 'beh', 'bih', 'boh']  # los corchetes indican los elementos que estan en
    # la lista.
    print(lista1)

    # acceder lista
    print(lista1)
    for elemento in lista1:
        print(elemento)

    # compara lista
    lista2 = ['beh', 'bah', 'boh', 'bih']
    if lista1 == lista2:
        print("Son identicas")

    # Longitud lista
    print(len(lista1))  # len es para medir los tamaños

    # Concatenar lista1
    print(lista1 + lista2)

    # numeros maximos y minimos de nueros. Max=5, Min=1
    lista3 = [1, 2, 3, 4, 5]
    print(max(lista3))
    print(min(lista3))

    # lista enlazada
    lista4 = ['Esto es', [1, 2, 3], 75]
    print(lista4)
    print(lista4[0])
    print(lista4[1])
    print(lista4[1][1])

    # Manipular datos
    lista5 = ['o', 'p', 'a']
    lista5.append('z')  # El append agrega un valor al final de la lista.
    print(lista5)
    lista5.insert(0, 'x')  # insert agrega los valosres al final de la lista.
    print(lista5)
    lista5.extend(lista3)  # extend agrega la lista3 a la lista5.
    print(lista5)
    lista5.remove('z')
    print(lista5)
    for i in range(5):
        lista5.pop()  # el pop elimina le ultimo valor de la lista
    print(lista5)
    lista5[0] = 's'
    print(lista5)

    # Replicar lista
    lista6 = [1, 2, 3]
    print(lista6 * 2)

    # Slicing
    lista7 = ['carimanola', 'pajarilla', 'bofe']
    print(lista7[1:2])  # el sclicing realiza una nueva lista, con los valores tomados
    # 1 es el primer valor (carimanola) y el segundo valor es N-1
    # 2-1 = 1 osea que imprime solamente Carimanola.

    lista8 = []
    for i in range(100):
        lista8.append(i)
    print(lista8[6:70])
    print(lista8[:50])
    print(lista8[25:])